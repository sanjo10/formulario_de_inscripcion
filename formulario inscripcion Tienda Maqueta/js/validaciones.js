
function validarEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function validar_clave(contrasenna) {
    if (contrasenna.length >= 8) {
        var mayuscula = false;
        var minuscula = false;
        var numero = false;
        var caracter_especial = false;

        for (var i = 0; i < contrasenna.length; i++) {
            if (contrasenna.charCodeAt(i) >= 65 && contrasenna.charCodeAt(i) <= 90) {
                mayuscula = true;
            }
            else if (contrasenna.charCodeAt(i) >= 97 && contrasenna.charCodeAt(i) <= 122) {
                minuscula = true;
            }
            else if (contrasenna.charCodeAt(i) >= 48 && contrasenna.charCodeAt(i) <= 57) {
                numero = true;
            }
            else {
                caracter_especial = true;
            }
        }
        if (mayuscula == true && minuscula == true && caracter_especial == true && numero == true) {
            return true;
        }
    }
    return false;
}

var form = document.registrar;

document.registrar.onsubmit = function (e) {
    var ready = false;

    if (form.nombre.value != "" && form.apellidos.value != "" && form.email.value != "" && form.password.value != "") {
        ready = true;
    } else {
        ready = false;
        alert("Hay algunos campos vacios");
        e.preventDefault();
    }

    if (ready) {
        if (validarEmail(form.email.value)) {
            if (!validar_clave(form.password.value)) {
                alert("La contraseña debe tener ocho caracteres como mínimo, Letras mayúsculas, Letras minúsculas, Números, Símbolos");
                form.password.focus();
                e.preventDefault();
            }
        } else {
            alert("El email no tiene un formato valido!");
            form.email.focus();
            e.preventDefault();
        }
    }

}